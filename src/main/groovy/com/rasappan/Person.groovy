package com.rasappan

import org.hibernate.annotations.GenericGenerator

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "PERSON")
class Person implements Serializable {

    @Id
    @GenericGenerator(name="personId" , strategy="increment")
    @GeneratedValue(generator="personId")
    long id

    String name

    String email
}

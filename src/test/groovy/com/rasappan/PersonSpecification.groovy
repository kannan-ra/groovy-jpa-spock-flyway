package com.rasappan

import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import java.sql.SQLException

class PersonSpecification extends Specification {

    protected static EntityManagerFactory emf
    protected static EntityManager em

    def setupSpec() throws FileNotFoundException, SQLException {
        emf = Persistence.createEntityManagerFactory("jpa-h2")
        em = emf.createEntityManager()
    }

    def cleanupSpec() {
        em.clear()
        em.close()
        emf.close()
    }

    def "Persist() should successfully save Person to DB"() {
        given:
        assert emf != null

        when:
        em.transaction.begin()
        em.persist(new Person(name: 'jpa-test', email: 'someone@gmail.com'))
        em.transaction.commit()

        then:
        def list = em.createQuery("select p from Person p").resultList
        assert list != null
        assert 1 == list.size()
        assert 'jpa-test' == list.first().name
    }
}

